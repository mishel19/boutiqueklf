package boutiqueklf.controller.compras.proveedores;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.controller.compras.BeanCompras;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.managers.ManagerDAO;
import boutiqueklf.model.proveedores.managers.ManagerProveedor;

@Named
@SessionScoped
public class BeanProveedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerProveedor mproveedores;
	
	private List<CompProveedor> listaProveedores;
	private CompProveedor nuevoProveedor;
	private CompProveedor edicionProveedor;
	
	public BeanProveedor() {
		// TODO Auto-generated constructor stub
	}
	
	public String actionMenuProveedores() {
		listaProveedores=mproveedores.findAllProveedor();
		return "Proveedores";
	}
	
	public String actionMenuNuevoProveedor() {
		nuevoProveedor=new CompProveedor();
		return "proveedor_nuevo";
	}
	
	public void actionListenerInsertarNuevoProveedor() {
		try {
			mproveedores.insertarProveedor(nuevoProveedor);
			listaProveedores=mproveedores.findAllProveedor();
			nuevoProveedor=new CompProveedor();
			JSFUtil.crearMensajeINFO("Proveedor insertado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String actionSeleccionarEdicionProveedor(CompProveedor proveedor) {
		edicionProveedor=proveedor;
		return "proveedor_edicion";
	}
	
	public void actionListenerActualizarEdicionProveedor() {
		try {
			mproveedores.actualizarProveedor(edicionProveedor);
			listaProveedores=mproveedores.findAllProveedor();
			JSFUtil.crearMensajeINFO("Proveedor actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarUsuario(int idProveedor) {
		try {
			mproveedores.eliminarProveedor(idProveedor);
			listaProveedores=mproveedores.findAllProveedor();
			JSFUtil.crearMensajeINFO("Proveedor eliminado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	
	public List<CompProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<CompProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public CompProveedor getNuevoProveedor() {
		return nuevoProveedor;
	}

	public void setNuevoProveedor(CompProveedor nuevoProveedor) {
		this.nuevoProveedor = nuevoProveedor;
	}

	public CompProveedor getEdicionProveedor() {
		return edicionProveedor;
	}

	public void setEdicionProveedor(CompProveedor edicionProveedor) {
		this.edicionProveedor = edicionProveedor;
	}
	
	
}
