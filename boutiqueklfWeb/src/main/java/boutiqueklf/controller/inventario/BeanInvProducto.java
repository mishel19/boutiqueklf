package boutiqueklf.controller.inventario;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.model.core.entities.CompProveedorDetalle;
import boutiqueklf.model.core.entities.InvProducto;
import boutiqueklf.model.core.entities.InvTalla;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.inventario.managers.ManagerInvProducto;
import boutiqueklf.model.ventas.managers.ManagerVenta;

@Named
@SessionScoped
public class BeanInvProducto implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerInvProducto managerInvProducto;

	private List<InvProducto> listaProductos;
	
	private List<CompProveedorDetalle> listaProveedorDetalle;
	private List<InvTalla> listaTalla;
	
	private int id_prod;
	private int id_inv_talla;
	private String nombre_producto;
	private String descripcion;
	private double precio;
	private double cantidad_total;
	
	@PostConstruct
	public void inicializarInvProducto() {
		listaProductos=managerInvProducto.findAllInvProducto();
	    listaProveedorDetalle=managerInvProducto.findAllCompProveedorDetalle();
	    listaTalla=managerInvProducto.findAllInvTalla();
	}
	
	public void actionlistenerInsertar() {
		//try {
	        managerInvProducto.crearInvProducto( id_inv_talla, nombre_producto, descripcion, precio, cantidad_total);
		    // inicializarInvProducto();
	         JSFUtil.crearMensajeINFO("Se ha insertado correctamente"); 
		//} catch (Exception e) {
			// TODO: handle exception
		  //  JSFUtil.crearMensajeERROR("Error al insertar"+ e.getMessage());
		//}
		
	}
	
	public void actionlistenerActulizar() {
		try {
			managerInvProducto.ingresarInvProducto(id_prod, id_inv_talla, nombre_producto, descripcion, precio, cantidad_total);
			//inicializarInvProducto();
			JSFUtil.crearMensajeINFO("Se ha actualizado");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR("No se ha actulizado"+e.getMessage());
		}
	}
	public void actionlistenerEliminarInvProducto(int id_prod) {
		try {
	      if (managerInvProducto.eliminarInvProducto(id_prod)) {
			  //inicializarInvProducto();
	    	  JSFUtil.crearMensajeINFO("Se ha eliminado");
		}else {
			JSFUtil.crearMensajeERROR("No se ha eliminado");
			
		}
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}
	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public List<CompProveedorDetalle> getListaProveedorDetalle() {
		return listaProveedorDetalle;
	}
	public void setListaProveedorDetalle(List<CompProveedorDetalle> listaProveedorDetalle) {
		this.listaProveedorDetalle = listaProveedorDetalle;
	}
	public List<InvTalla> getListaTalla() {
		return listaTalla;
	}
	public void setListaTalla(List<InvTalla> listaTalla) {
		this.listaTalla = listaTalla;
	}
	public int getId_prod() {
		return id_prod;
	}
	public void setId_prod(int id_prod) {
		this.id_prod = id_prod;
	}
	public int getId_inv_talla() {
		return id_inv_talla;
	}
	public void setId_inv_talla(int id_inv_talla) {
		this.id_inv_talla = id_inv_talla;
	}
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getCantidad_total() {
		return cantidad_total;
	}
	public void setCantidad_total(double cantidad_total) {
		this.cantidad_total = cantidad_total;
	}

	
	
}
