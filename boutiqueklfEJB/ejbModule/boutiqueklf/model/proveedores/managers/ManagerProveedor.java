package boutiqueklf.model.proveedores.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import boutiqueklf.model.auditoria.managers.ManagerAuditoria;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.SegModulo;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.managers.ManagerDAO;
import boutiqueklf.model.proveedores.dtos.DTOProveeedor;
import boutiqueklf.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

    /**
     * Default constructor. 
     */
    public ManagerProveedor() {
        // TODO Auto-generated constructor stub
    }
    
    @SuppressWarnings("unchecked")
	public List<CompProveedor> findAllProveedor(){
    	return mDAO.findAll(CompProveedor.class, "apellidos");
    }
    
    public void insertarProveedor(CompProveedor nuevoProveedor) throws Exception {
    	nuevoProveedor.getRucCompProveedor();
    	nuevoProveedor.getNombreProve();
		nuevoProveedor.getApellidoProve();
		nuevoProveedor.getDireccionProve();
		nuevoProveedor.getTelefonoProve();
		nuevoProveedor.getCorreoPrive();
    	mDAO.insertar(nuevoProveedor);
    }
    
    public void actualizarProveedor(CompProveedor edicionProveedor) throws Exception {
    	CompProveedor proveedor=(CompProveedor) mDAO.findById(CompProveedor.class, edicionProveedor.getRucCompProveedor());
    	proveedor.setNombreProve(edicionProveedor.getNombreProve());
    	proveedor.setApellidoProve(edicionProveedor.getApellidoProve());
    	proveedor.setDireccionProve(edicionProveedor.getDireccionProve());
    	proveedor.setTelefonoProve(edicionProveedor.getTelefonoProve());
    	proveedor.setCorreoPrive(edicionProveedor.getCorreoPrive());
    	mDAO.actualizar(proveedor);
    	mAuditoria.mostrarLog(getClass(), "actualizarProvedor", "se actualizó al proveedor "+proveedor.getApellidoProve());
    }

    public void eliminarProveedor(int IdProveedor) throws Exception {
    	mDAO.eliminar(CompProveedor.class, IdProveedor);
    }
}
