package boutiqueklf.model.ventas.managers;

import java.math.BigDecimal;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import boutiqueklf.model.core.entities.CliCliente;
import boutiqueklf.model.core.entities.InvProducto;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.core.entities.VenFacturaCabecera;
import boutiqueklf.model.core.entities.VenFacturaDetalle;
import boutiqueklf.model.ventas.dtos.DTODetalleVenta;


/**
 * Session Bean implementation class ManagerVenta
 */
@Stateless
@LocalBean
public class ManagerVenta {
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */	
    public ManagerVenta() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvProducto> findAllInvProducto(){
    	TypedQuery<InvProducto> q = em.createQuery("select m from InvProducto m", InvProducto.class);
    	return q.getResultList();    
    }
    public List<CliCliente> findAllCliCliente(){
    	TypedQuery<CliCliente> q = em.createQuery("select m from CliCliente m", CliCliente.class);
    	return q.getResultList();    
    }
    public List<ThmEmpleado> findAllThmEmpleado(){
    	TypedQuery<ThmEmpleado> q = em.createQuery("select m from ThmEmpleado m", ThmEmpleado.class);
    	return q.getResultList();    
    }
    public List<VenFacturaCabecera> findAllVenFacturaCAbecera(){
    	TypedQuery<VenFacturaCabecera> q = em.createQuery("select m from VenFacturaCabecera m order by idVenFacturaCabecera", VenFacturaCabecera.class);
    	return q.getResultList();    
    }
    
    public List<DTODetalleVenta> eliminarProducto(List<DTODetalleVenta> detalleVenta, int idProducto){
    	if(detalleVenta==null)
    		return null;
    	int i=0;
    	for(DTODetalleVenta p:detalleVenta) {
    		if(p.getIdProducto()==idProducto) {
    			detalleVenta.remove(i);
    			break;
    		}
    		i++;
    	}
    	return detalleVenta;
    }
    
//Maestro detalle ventas
    
    public DTODetalleVenta crearDetalleVenta(int idProducto,int cantidad) {
    	InvProducto a=em.find(InvProducto.class, idProducto);
    	
    	DTODetalleVenta detalle=new DTODetalleVenta();
    	detalle.setIdProducto(idProducto);
    	detalle.setCantidad(cantidad);
    	detalle.setNombre(a.getNombreProducto());    	
    	detalle.setDescripcion(a.getDescripcion()+" "+a.getInvTalla().getNombreTalla());    	
    	detalle.setPrecio((a.getPrecio().doubleValue()));
    	detalle.setTotal(a.getPrecio().doubleValue()*cantidad);
    	
    	return detalle;
    }
    public double subtotalDetalle(List<DTODetalleVenta> listaDetalleVenta) {
    	double total=0;
    	for(DTODetalleVenta d:listaDetalleVenta)
    		total+=d.getTotal();
    	return total;
    }
    public double iva(List<DTODetalleVenta> listaDetalleVenta){
    	double iva=0;
    	double total=0;
    	for(DTODetalleVenta d:listaDetalleVenta)
    		total+=d.getTotal();
    	    iva=total*0.12;
    	return iva;
    }
    public double descuento(List<DTODetalleVenta> listaDetalleVenta){
    	double descuento=0;
    	double total=0;
    	for(DTODetalleVenta d:listaDetalleVenta)
    		total+=d.getTotal();
    	if(total >= 40) {
    		descuento=(total*20)/100;
    	}else {
    		descuento=0;
    	}
    	return descuento;
    }
    public double totalApagar(List<DTODetalleVenta> listaDetalleVenta){
    	double descuento=0;
    	double total=0;
    	double totalApagar=0;
    	for(DTODetalleVenta d:listaDetalleVenta)
    		total+=d.getTotal();
    	if(total >= 40) {
    		descuento=(total*20)/100;
    	}else {
    		descuento=0;
    	}
    	
    	totalApagar= ((total*0.12)+total)-descuento;
    	return totalApagar;
    }
    
    
    public void ventaMultiple(List<DTODetalleVenta> listaDetalleVenta,String cedulaCliente, int idEmpleado) {
    	
    	CliCliente cliente=em.find(CliCliente.class, cedulaCliente);
    	ThmEmpleado empleado=em.find(ThmEmpleado.class, idEmpleado);
    	
    	VenFacturaCabecera facturaCabecera=new VenFacturaCabecera();
    	facturaCabecera.setCliCliente(cliente);
    	facturaCabecera.setThmEmpleado(empleado);
    	
    	facturaCabecera.setFecha(new Date());
    	facturaCabecera.setSubtotal(new BigDecimal(subtotalDetalle(listaDetalleVenta)));
    	facturaCabecera.setIva(new BigDecimal(0.12 * (subtotalDetalle(listaDetalleVenta))));    	
    	facturaCabecera.setDescuento(new BigDecimal(descuento(listaDetalleVenta)));
    	facturaCabecera.setTotal(new BigDecimal(subtotalDetalle(listaDetalleVenta)+(0.12 * (subtotalDetalle(listaDetalleVenta)))-descuento(listaDetalleVenta)));
    	
    	List<VenFacturaDetalle> listaDetalle=new ArrayList<VenFacturaDetalle>();
    	facturaCabecera.setVenFacturaDetalles(listaDetalle);
    	
    	for(DTODetalleVenta det:listaDetalleVenta) {
    		VenFacturaDetalle facturaDetalle=new VenFacturaDetalle();    		
    		InvProducto producto=em.find(InvProducto.class, det.getIdProducto());
    		facturaDetalle.setVenFacturaCabecera(facturaCabecera);
    		facturaDetalle.setInvProducto(producto);
    		facturaDetalle.setCantidad(new BigDecimal(det.getCantidad()));
    		facturaDetalle.setTotal(new BigDecimal(det.getTotal()));
    		listaDetalle.add(facturaDetalle);
    	}
    	
    	em.persist(facturaCabecera);
    }
     
    //buscar por fecha
    public VenFacturaCabecera findVentaByFecha(String fecha) throws Exception {
		List<VenFacturaCabecera> listaFacturaCab = findAllVenFacturaCAbecera();
		VenFacturaCabecera fechaNuevo = new VenFacturaCabecera();
		boolean a = false;
		System.out.println(fecha);
		for (VenFacturaCabecera cl : listaFacturaCab) {
			if (cl.getFecha().equals(fecha)) {
				fechaNuevo = cl;
				a = true;
				System.out.println("cliente encontrado: " + cl.getFecha());
				break;
			}
		}
		if (!a)
			throw new Exception("Fecha no encontrada");

		return fechaNuevo;
	}
   

}
